/*******************************************************************************
**
** Copyright (C) 2021-2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the QR Code Reader project.
**
** Redistribution and use in source and binary forms,
** with or without modification, are permitted provided
** that the following conditions are met:
**
** * Redistributions of source code must retain the above copyright notice,
**   this list of conditions and the following disclaimer.
** * Redistributions in binary form must reproduce the above copyright notice,
**   this list of conditions and the following disclaimer
**   in the documentation and/or other materials provided with the distribution.
** * Neither the name of the copyright holder nor the names of its contributors
**   may be used to endorse or promote products derived from this software
**   without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
** IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
** PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS;
** OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
** WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
** EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
*******************************************************************************/

import QtQuick 2.6
import QtPositioning 5.2
import Sailfish.Silica 1.0
import ru.auroraos.QrCodeReader 1.0

Dialog {
    objectName: "processingPage"

    property alias qrCodeData: qrCodeHandler.sourceText

    function acceptIsAvail(handler) {
        return handler.executable || handler.type == QRCodeType.Geo;
    }

    function acceptBtnText(handler) {
        if (handler.executable) {
            return qsTr("Execute");
        }

        if (handler.type == QRCodeType.Geo) {
            return qsTr("Show");
        }

        return "";
    }

    function actionOnAccept(handler) {
        if (handler.executable) {
            handler.execute();
        }

        if (handler.type == QRCodeType.Geo) {
            pageStack.push(Qt.resolvedUrl("MapPage.qml"), {
                               mapPosition: QtPositioning.coordinate(handler.fields["latitude"], handler.fields["longitude"]),
                               mapZoomLevel: 19
                           });
        }
    }

    forwardNavigation: acceptIsAvail(qrCodeHandler)

    onAccepted: actionOnAccept(qrCodeHandler)

    QRCodeHandler {
        id: qrCodeHandler

        objectName: "qrCodeHandler"
    }

    SilicaFlickable {
        objectName: "flickable"
        anchors.fill: parent
        contentHeight: column.height

        VerticalScrollDecorator {
            objectName: "scrollDecorator"
        }

        Column {
            id: column

            objectName: "column"
            width: parent.width
            spacing: Theme.paddingLarge

            DialogHeader {
                objectName: "pageHeader"
                cancelText: qsTr("Back")
                acceptText: acceptBtnText(qrCodeHandler)
                acceptTextVisible: acceptIsAvail(qrCodeHandler)
            }

            SectionHeader {
                objectName: "sourceTextSection"
                text: qsTr("Source text")
            }

            TextArea {
                objectName: "sourceText"
                anchors {
                    left: parent.left
                    right: parent.right
                }
                height: implicitHeight
                readOnly: true
                text: qrCodeHandler.sourceText
            }

            SectionHeader {
                objectName: "formatedTextSection"
                text: qsTr("Formated text")
            }

            TextArea {
                objectName: "formatedText"
                anchors {
                    left: parent.left
                    right: parent.right
                }
                height: implicitHeight
                readOnly: true
                text: qrCodeHandler.formatedText
            }
        }
    }
}
