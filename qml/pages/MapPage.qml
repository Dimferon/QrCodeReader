/*******************************************************************************
**
** Copyright (C) 2021-2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the QR Code Reader project.
**
** Redistribution and use in source and binary forms,
** with or without modification, are permitted provided
** that the following conditions are met:
**
** * Redistributions of source code must retain the above copyright notice,
**   this list of conditions and the following disclaimer.
** * Redistributions in binary form must reproduce the above copyright notice,
**   this list of conditions and the following disclaimer
**   in the documentation and/or other materials provided with the distribution.
** * Neither the name of the copyright holder nor the names of its contributors
**   may be used to endorse or promote products derived from this software
**   without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
** IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
** PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS;
** OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
** WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
** EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
*******************************************************************************/

import QtQuick 2.6
import QtLocation 5.0
import QtPositioning 5.2
import Sailfish.Silica 1.0

Page {
    objectName: "mapPage"

    property var mapPosition
    property int mapZoomLevel

    Plugin {
        id: mapPlugin

        objectName: "mapPlugin"
        name: "webtiles"
        allowExperimental: false

        PluginParameter {
            objectName: "schemeParameter"
            name: "webtiles.scheme"
            value: "https"
        }

        PluginParameter {
            objectName: "hostParameter"
            name: "webtiles.host"
            value: "tile.openstreetmap.org"
        }

        PluginParameter {
            objectName: "pathParameter"
            name: "webtiles.path"
            value: "/${z}/${x}/${y}.png"
        }
    }

    PageHeader {
        id: pageHeader

        objectName: "pageHeader"
        title: qsTr("Map Viewer")
    }

    Map {
        objectName: "map"
        anchors {
            top: pageHeader.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        plugin: mapPlugin

        Component.onCompleted: {
            center = QtPositioning.coordinate(mapPosition.latitude, mapPosition.longitude);
            zoomLevel = mapZoomLevel;
        }

        MapQuickItem {
            objectName: "mapItem"
            coordinate: QtPositioning.coordinate(mapPosition.latitude, mapPosition.longitude)
            anchorPoint.x: image.width / 2
            anchorPoint.y: image.height
            sourceItem: Image {
                id: image

                width: 64
                height: 64
                source: "image://theme/icon-m-whereami?darkRed"
            }
        }
    }
}
