/*******************************************************************************
**
** Copyright (C) 2021-2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the QR Code Reader project.
**
** Redistribution and use in source and binary forms,
** with or without modification, are permitted provided
** that the following conditions are met:
**
** * Redistributions of source code must retain the above copyright notice,
**   this list of conditions and the following disclaimer.
** * Redistributions in binary form must reproduce the above copyright notice,
**   this list of conditions and the following disclaimer
**   in the documentation and/or other materials provided with the distribution.
** * Neither the name of the copyright holder nor the names of its contributors
**   may be used to endorse or promote products derived from this software
**   without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
** IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
** PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS;
** OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
** WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
** EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
*******************************************************************************/

#ifndef QRCODEHANDLER_H
#define QRCODEHANDLER_H

#include <QtCore/QObject>
#include <QtCore/QSharedPointer>

#include "qrcodetypes.h"

class QRCodeHandlerPrivate;

class QRCodeHandler : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QRCodeType type READ type NOTIFY typeChanged)
    Q_PROPERTY(QRCodeText sourceText READ sourceText WRITE setSourceText NOTIFY sourceTextChanged)
    Q_PROPERTY(QRCodeText formatedText READ formatedText NOTIFY formatedTextChanged)
    Q_PROPERTY(QRCodeFields fields READ fields NOTIFY fieldsChanged)
    Q_PROPERTY(bool executable READ executable NOTIFY executableChanged)

public:
    explicit QRCodeHandler(QObject *parent = nullptr);

    void setSourceText(const QRCodeText &sourceText);

    QRCodeType type() const;
    QRCodeText sourceText() const;
    QRCodeText formatedText() const;
    QRCodeFields fields() const;
    bool executable() const;

    Q_INVOKABLE bool execute();

signals:
    void typeChanged(QRCodeType type);
    void sourceTextChanged(const QRCodeText &sourceText);
    void formatedTextChanged(const QRCodeText &formatedText);
    void fieldsChanged(const QRCodeFields &fields);
    void executableChanged(bool executable);

private:
    QSharedPointer<QRCodeHandlerPrivate> m_data { nullptr };
};

#endif // QRCODEHANDLER_H
