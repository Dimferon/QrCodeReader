/*******************************************************************************
**
** Copyright (C) 2021-2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the QR Code Reader project.
**
** Redistribution and use in source and binary forms,
** with or without modification, are permitted provided
** that the following conditions are met:
**
** * Redistributions of source code must retain the above copyright notice,
**   this list of conditions and the following disclaimer.
** * Redistributions in binary form must reproduce the above copyright notice,
**   this list of conditions and the following disclaimer
**   in the documentation and/or other materials provided with the distribution.
** * Neither the name of the copyright holder nor the names of its contributors
**   may be used to endorse or promote products derived from this software
**   without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
** IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
** PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS;
** OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
** WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
** EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
*******************************************************************************/

#include <QtCore/QDir>
#include <QtCore/QUrl>
#include <QtCore/QFile>
#include <QtGui/QDesktopServices>

#include "qrcodehandler_p.h"
#include "qrcodehandler.h"

static const QString emailStart = QStringLiteral("mailto:");
static const QString linkV0Start = QStringLiteral("http://");
static const QString linkV1Start = QStringLiteral("https://");
static const QString phoneStart = QStringLiteral("tel:");
static const QString smsStart = QStringLiteral("smsto:");
static const QString calendarStart = QStringLiteral("begin:vcalendar");
static const QString calendarEnd = QStringLiteral("end:vcalendar");
static const QString vcardStart = QStringLiteral("begin:vcard");
static const QString vcardEnd = QStringLiteral("end:vcard");
static const QString geoStart = QStringLiteral("geo:");

static const QMap<QRCodeType, QString> typeNames = {
    { QRCodeType::Text, QStringLiteral("Text") },
    { QRCodeType::Link, QStringLiteral("Link") },
    { QRCodeType::Phone, QStringLiteral("Phone") },
    { QRCodeType::EMail, QStringLiteral("E-Mail") },
    { QRCodeType::VCard, QStringLiteral("V-Card") },
    { QRCodeType::Sms, QStringLiteral("SMS") },
    { QRCodeType::Geo, QStringLiteral("Geo") },
};

QRCodeHandlerPrivate::QRCodeHandlerPrivate(QObject *parent)
    : QObject(parent)
{
}

void QRCodeHandlerPrivate::setSourceText(const QRCodeText &sourceText)
{
    if (m_sourceText == sourceText)
        return;

    m_sourceText = sourceText;
    emit sourceTextChanged(sourceText);

    QRCodeType type = _determineType(sourceText);
    if (m_type != type) {
        m_type = type;
        emit typeChanged(type);
    }

    QRCodeFields fields = _parseSourceText(type, sourceText);
    if (m_fields != fields) {
        m_fields = fields;
        emit fieldsChanged(fields);
    }

    QRCodeText formatedText = _makeFormatedText(type, fields);
    if (m_formatedText != formatedText) {
        m_formatedText = formatedText;
        emit formatedTextChanged(formatedText);
    }

    bool executable = _executableCheck(type, fields);
    if (m_executable != executable) {
        m_executable = executable;
        emit executableChanged(executable);
    }
}

QRCodeType QRCodeHandlerPrivate::type() const
{
    return m_type;
}

QRCodeText QRCodeHandlerPrivate::sourceText() const
{
    return m_sourceText;
}

QRCodeText QRCodeHandlerPrivate::formatedText() const
{
    return m_formatedText;
}

QRCodeFields QRCodeHandlerPrivate::fields() const
{
    return m_fields;
}

bool QRCodeHandlerPrivate::executable() const
{
    return m_executable;
}

QRCodeType QRCodeHandlerPrivate::_determineType(const QRCodeText &sourceText)
{
    auto startsWith = [sourceText](const QString &text) {
        return sourceText.startsWith(text, Qt::CaseInsensitive);
    };

    auto endsWith = [sourceText](const QString &text) {
        return sourceText.endsWith(text, Qt::CaseInsensitive);
    };

    if (startsWith(emailStart))
        return QRCodeType::EMail;
    else if (startsWith(linkV0Start) || startsWith(linkV1Start))
        return QRCodeType::Link;
    else if (startsWith(phoneStart))
        return QRCodeType::Phone;
    else if (startsWith(smsStart))
        return QRCodeType::Sms;
    else if (startsWith(vcardStart) && endsWith(vcardEnd))
        return QRCodeType::VCard;
    else if (startsWith(geoStart))
        return  QRCodeType::Geo;
    else
        return QRCodeType::Text;
}

QRCodeText QRCodeHandlerPrivate::_makeFormatedText(QRCodeType type, const QRCodeFields &fields)
{
    QStringList lines;
    lines.prepend(QStringLiteral("[ Type: %1 ]\n").arg(typeNames.value(type)));
    QMapIterator<QString, QVariant> it(fields);
    while (it.hasNext()) {
        it.next();
        lines.append(QStringLiteral("%1: %2").arg(it.key(), it.value().value<QString>()));
    }

    return lines.join(QStringLiteral("\n"));
}

QRCodeFields QRCodeHandlerPrivate::_parseSourceText(QRCodeType type, const QRCodeText &sourceText)
{
    if (type == QRCodeType::Text)
        return _parseText(sourceText);
    else if (type == QRCodeType::EMail)
        return _parseEMail(sourceText);
    else if (type == QRCodeType::Link)
        return _parseLink(sourceText);
    else if (type == QRCodeType::Phone)
        return _parsePhone(sourceText);
    else if (type == QRCodeType::Sms)
        return _parseSms(sourceText);
    else if (type == QRCodeType::VCard)
        return _parseVCard(sourceText);
    else if (type == QRCodeType::Geo)
        return _parseGeo(sourceText);
    else
        return {  };
}

QRCodeFields QRCodeHandlerPrivate::_parseText(const QRCodeText &sourceText)
{
    // "Here can be everything"

    QRCodeFields fields;
    fields.insert(QStringLiteral("data"), sourceText);

    return fields;
}

QRCodeFields QRCodeHandlerPrivate::_parseEMail(const QRCodeText &sourceText)
{
    // "mailto:email@example.com?subject=Тема&body=Сообщение"

    static const QString subjectStart = QStringLiteral("subject=");
    static const QString bodyStart = QStringLiteral("body=");

    QString copyText = sourceText;
    copyText = copyText.remove(0, emailStart.length());

    QRCodeFields fields;
    QStringList dataList = copyText.split(QStringLiteral("?"));
    if (dataList.size() == 2) {
        fields.insert(QStringLiteral("address"), dataList.first());
        dataList = dataList.last().split(QStringLiteral("&"));

        for (auto &&dataItem : dataList) {
            if (dataItem.startsWith(subjectStart, Qt::CaseInsensitive))
                fields.insert(QStringLiteral("subject"), dataItem.remove(0, subjectStart.length()));
            else if (dataItem.startsWith(bodyStart, Qt::CaseInsensitive))
                fields.insert(QStringLiteral("body"), dataItem.remove(0, bodyStart.length()));
        }
    }

    return fields;
}

QRCodeFields QRCodeHandlerPrivate::_parseLink(const QRCodeText &sourceText)
{
    // "https://twitter.com/elonmusk"

    QRCodeFields fields;
    fields.insert(QStringLiteral("link"), sourceText);

    return fields;
}

QRCodeFields QRCodeHandlerPrivate::_parsePhone(const QRCodeText &sourceText)
{
    // "tel:+7 (495) 000-00-00"

    QString copyText = sourceText;
    copyText = copyText.remove(QRegExp(QStringLiteral("[^0-9]")));

    QRCodeFields fields;
    fields.insert(QStringLiteral("number"), copyText);

    return fields;
}

QRCodeFields QRCodeHandlerPrivate::_parseSms(const QRCodeText &sourceText)
{
    // "smsto:+7 (495) 000-00-00:Тестовое сообщение."

    QString copyText = sourceText;
    copyText = copyText.remove(0, smsStart.length());

    QRCodeFields fields;
    QStringList dataList = copyText.split(QStringLiteral(":"));
    if (dataList.size() == 2) {
        fields.insert(QStringLiteral("number"), dataList.first().remove(QRegExp(QStringLiteral("[^0-9]"))));
        fields.insert(QStringLiteral("message"), dataList.last());
    }

    return fields;
}

QRCodeFields QRCodeHandlerPrivate::_parseVCard(const QRCodeText &sourceText)
{
    // "BEGIN:VCARD\r\nVERSION:2.1\r\nN:Ivanov Ivan Ivanovich\r\nTEL;HOME;VOICE:0043-7252-72720\r\nTEL;WORK;VOICE:0043-7252-72720\r\nEMAIL:email@example.com\r\nORG:Google\r\nTITLE:Developer\r\nBDAY:20210920\r\nADR:Mountain View\r\nURL:https://www.google.ru\r\nEND:VCARD"

    static const QString versionStart = QStringLiteral("version:");
    static const QString nameStart = QStringLiteral("n:");
    static const QString telHomeVoiceStart = QStringLiteral("TEL;HOME;VOICE:");
    static const QString telWorkVoiceStart = QStringLiteral("TEL;WORK;VOICE:");
    static const QString emailStart = QStringLiteral("email:");
    static const QString orgStart = QStringLiteral("org:");
    static const QString titleStart = QStringLiteral("title:");
    static const QString bdayStart = QStringLiteral("bday:");
    static const QString adrStart = QStringLiteral("adr:");
    static const QString urlStart = QStringLiteral("url:");

    QString copyText = sourceText;
    copyText = copyText.remove(0, vcardStart.length());
    copyText = copyText.remove(copyText.lastIndexOf(vcardEnd), vcardEnd.length());

    QRCodeFields fields;
    QStringList dataList = copyText.split(QStringLiteral("\n"));
    for (auto &&dataItem : dataList) {
        dataItem = dataItem.simplified();
        if (dataItem.startsWith(versionStart, Qt::CaseInsensitive))
            fields.insert(QStringLiteral("version"), dataItem.remove(0, versionStart.length()));
        else if (dataItem.startsWith(nameStart, Qt::CaseInsensitive))
            fields.insert(QStringLiteral("name"), dataItem.remove(0, nameStart.length()));
        else if (dataItem.startsWith(telHomeVoiceStart, Qt::CaseInsensitive))
            fields.insert(QStringLiteral("number (home)"), dataItem.remove(0, telHomeVoiceStart.length()));
        else if (dataItem.startsWith(telWorkVoiceStart, Qt::CaseInsensitive))
            fields.insert(QStringLiteral("number (work)"), dataItem.remove(0, telWorkVoiceStart.length()));
        else if (dataItem.startsWith(emailStart, Qt::CaseInsensitive))
            fields.insert(QStringLiteral("email"), dataItem.remove(0, emailStart.length()));
        else if (orgStart.startsWith(orgStart, Qt::CaseInsensitive))
            fields.insert(QStringLiteral("organization"), dataItem.remove(0, orgStart.length()));
        else if (dataItem.startsWith(titleStart, Qt::CaseInsensitive))
            fields.insert(QStringLiteral("position"), dataItem.remove(0, titleStart.length()));
        else if (dataItem.startsWith(bdayStart, Qt::CaseInsensitive))
            fields.insert(QStringLiteral("birthday"), dataItem.remove(0, bdayStart.length()));
        else if (dataItem.startsWith(adrStart, Qt::CaseInsensitive))
            fields.insert(QStringLiteral("address"), dataItem.remove(0, adrStart.length()));
        else if (dataItem.startsWith(urlStart, Qt::CaseInsensitive))
            fields.insert(QStringLiteral("url"), dataItem.remove(0, urlStart.length()));
    }

    return fields;
}

QRCodeFields QRCodeHandlerPrivate::_parseGeo(const QRCodeText &sourceText)
{
    // geo:40.742081,-74.004480

    QString copyText = sourceText;
    copyText = copyText.remove(0, geoStart.length());

    QRCodeFields fields;
    QStringList dataList = copyText.split(QStringLiteral(","));
    if (dataList.size() == 2) {
        fields.insert(QStringLiteral("latitude"), dataList.first());
        fields.insert(QStringLiteral("longitude"), dataList.last());
    }

    return fields;
}

bool QRCodeHandlerPrivate::_executableCheck(QRCodeType type, const QRCodeFields &fields)
{
    if (type == QRCodeType::Text)
        return false;
    else if (type == QRCodeType::EMail)
        return !fields.value(QStringLiteral("address")).isNull();
    else if (type == QRCodeType::Link)
        return !fields.value(QStringLiteral("link")).isNull();
    else if (type == QRCodeType::Phone)
        return !fields.value(QStringLiteral("number")).isNull();
    else if (type == QRCodeType::Sms)
        return !fields.value(QStringLiteral("number")).isNull();
    else if (type == QRCodeType::VCard)
        return !fields.value(QStringLiteral("name")).isNull();
    else if (type == QRCodeType::Geo)
        return false;
    else
        return false;
}

bool QRCodeHandlerPrivate::execute()
{
    if (m_type == QRCodeType::Text)
        return _executeText();
    else if (m_type == QRCodeType::EMail)
        return _executeEMail();
    else if (m_type == QRCodeType::Link)
        return _executeLink();
    else if (m_type == QRCodeType::Phone)
        return _executePhone();
    else if (m_type == QRCodeType::Sms)
        return _executeSms();
    else if (m_type == QRCodeType::VCard)
        return _executeVCard();
    else if (m_type == QRCodeType::Geo)
        return _executeGeo();
    else
        return false;
}

bool QRCodeHandlerPrivate::_executeText()
{
    return false;
}

bool QRCodeHandlerPrivate::_executeEMail()
{
    return QDesktopServices::openUrl(QUrl(m_sourceText));
}

bool QRCodeHandlerPrivate::_executeLink()
{
    return QDesktopServices::openUrl(QUrl(m_sourceText));
}

bool QRCodeHandlerPrivate::_executePhone()
{
    return QDesktopServices::openUrl(QUrl(m_sourceText));
}

bool QRCodeHandlerPrivate::_executeSms()
{
    const QString number = m_fields.value(QStringLiteral("number")).value<QString>();
    const QString message = m_fields.value(QStringLiteral("message")).value<QString>();
    const QString smsUrl = QStringLiteral("sms:%1?body=%2").arg(number, message);

    return QDesktopServices::openUrl(QUrl(smsUrl));
}

bool QRCodeHandlerPrivate::_executeVCard()
{
    QDir dir(QStandardPaths::writableLocation(QStandardPaths::TempLocation));
    if (!dir.exists())
        dir.mkpath(dir.absolutePath());

    QFile file(dir.absoluteFilePath(QStringLiteral("profile.vcard")));
    if (file.open(QFile::WriteOnly | QFile::Truncate | QFile::Text)) {
        file.write(m_sourceText.toUtf8());
        file.close();
    }

    return QDesktopServices::openUrl(QUrl::fromLocalFile(file.fileName()));
}

bool QRCodeHandlerPrivate::_executeGeo()
{
    return false;
}

QRCodeHandler::QRCodeHandler(QObject *parent)
    : QObject(parent), m_data(new QRCodeHandlerPrivate(this))
{
    connect(m_data.data(), &QRCodeHandlerPrivate::typeChanged,
            this, &QRCodeHandler::typeChanged);
    connect(m_data.data(), &QRCodeHandlerPrivate::sourceTextChanged,
            this, &QRCodeHandler::sourceTextChanged);
    connect(m_data.data(), &QRCodeHandlerPrivate::formatedTextChanged,
            this, &QRCodeHandler::formatedTextChanged);
    connect(m_data.data(), &QRCodeHandlerPrivate::fieldsChanged,
            this, &QRCodeHandler::fieldsChanged);
    connect(m_data.data(), &QRCodeHandlerPrivate::executableChanged,
            this, &QRCodeHandler::executableChanged);
}

void QRCodeHandler::setSourceText(const QRCodeText &sourceText)
{
    m_data->setSourceText(sourceText);
}

QRCodeType QRCodeHandler::type() const
{
    return m_data->type();
}

QRCodeText QRCodeHandler::sourceText() const
{
    return m_data->sourceText();
}

QRCodeText QRCodeHandler::formatedText() const
{
    return m_data->formatedText();
}

QRCodeFields QRCodeHandler::fields() const
{
    return m_data->fields();
}

bool QRCodeHandler::executable() const
{
    return m_data->executable();
}

bool QRCodeHandler::execute()
{
    return m_data->execute();
}
