# QR Code Reader

The project provides an example of using video filters to process QR codes.

The main purpose is to show not only what features are available to work with these API,
but also how to use them correctly.

The source code of the project is provided under [the license](LICENSE.BSD-3-CLAUSE.md),
that allows it to be used in third-party applications.

The [contributor agreement](CONTRIBUTING.md) documents the rights granted by contributors
to the Open Mobile Platform.

[Code of conduct](CODE_OF_CONDUCT.md) is a current set of rules of the Open Mobile
Platform which informs you how we expect the members of the community will interact
while contributing and communicating.

For information about contributors see [AUTHORS](AUTHORS.md).

## Project Structure

The project has a common structure of an application based on C++ and QML for Aurora OS.
All the C++ sources files are in [src](src) directory, QML sources are in [qml](qml) directory.

The features related to the examples are the following:
- The pictures used for the UI are located in the [qml/images](qml/images) directory.
- QML documents of application pages are located in the [qml/pages](qml/pages) directory.
- Information about the project and the license is displayed in [AboutPage.qml](qml/pages/AboutPage.qml) file.
- The QR code is captured in the [RecognitionPage.qml](qml/pages/RecognitionPage.qml) file.
- The information about the QR code is displayed on the [ProcessingPage.qml](qml/pages/ProcessingPage.qml).
- The geolocation output encoded in the QR code is output in the [MapPage.qml](qml/pages/MapPage.qml) file.

## Supported data types in QR codes

This application supports such types of data as: email address, link, phone number, SMS message,
contact, geolocation and plain text.

Pre-generated QR codes for testing are located in the [test-qr-codes](test-qr-codes) directory.
A website was used to generate them https://qrcode.tec-it.com/ru/Raw/.
